# property-get

Intelligently get properties from an object using a string with dot-notation and a fallback.

## Installation
```sh
npm i -S prop-get
```

## Simple Example
This module was specifically written for pulling data from objects which are multiple levels down and may or may not exist.

Take `data.user.profile.roles.isAdmin` as an example.

Let us say that the `roles` is missing from the data.  You would get an error whining that isAdmin doesn't exist on unknown.

property-get is intended to alleviate that.

```js
import propertyGet from 'property-get'

// checks site.location, falls back to `null`
const siteLocation = propertyGet(site, 'location')

// checks device.region.value, falls back to `null`
const region = propertyGet(device, 'region.value')

// Checks for the value of isAdmin, and if any of those 
// properties doesn't exist it falls back to `false`
const isAdmin = propertyGet(data, 'user.profile.roles.isAdmin', false)
      

```
