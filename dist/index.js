const stringPathGet = (targetObjectRoot, stringPath, fallbackValue = null) => {
  let idxFirstDot = stringPath.indexOf('.');

  const property = stringPath.substr(0, idxFirstDot),
        restOfPath = stringPath.substr(idxFirstDot + 1);

  let returnedValue = fallbackValue;

  if (!targetObjectRoot) return fallbackValue;
  if (property && restOfPath && property !== '') returnedValue = stringPathGet(targetObjectRoot[property], restOfPath, fallbackValue);else if (!property && restOfPath && restOfPath !== '') {
    if (targetObjectRoot[restOfPath] === null) returnedValue = fallbackValue;else returnedValue = targetObjectRoot[restOfPath];
  }

  return returnedValue;
};

module.exports = stringPathGet;
//# sourceMappingURL=index.js.map